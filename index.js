const builder = require('botbuilder')
const messengerAi = require('botbuilder-messenger')
const localConf = require('./conf.json')

const connect = new builder.ChatConnector({
  appId: messengerAppId,
  appPassword: messengerPassword,
})

const tableName = 'botdata'
const messengerInit = new messengerConnect(tableName)

const dialogs = require('require-all')(__dirname + '/dialogs')

const _initialize = (server) => {

  server.post('/api/messages', connector.listen())

  const bot = new builder.UniversalBot(connector, (session, args) => {

    if(!session.userData.luisEnabled) {
      session.beginDialog('/menu')
    }
  })
  bot.set('storage', tableStorage)

  const menuDialog = dialogs.menu.init(bot)
  const changeLanguageDialog = dialogs.changeLanguage.init(bot)
  const requestInfoDialog = dialogs.requestInfo.init(bot)

  chat.initialize(bot)
  
  bot.customAction({
    matches: /exit/gi,
    onSelectAction: (session, args, next) => {
      session.userData.luisEnabled = false
      session.reset()
    }
  })
}

module.exports = {
  initialize: _initialize
}