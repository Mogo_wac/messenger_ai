const _ = require('lodash')
const builder = require('botbuilder')

const changeLanguage = (bot) => {

  const languagesSupported = [
    { key: 'en', value: 'english' },
    { key: 'fr', value: 'french' }
  ]

  return bot.dialog('/changeLanguage', [
    (session) => {
      builder.Prompts.choice(session, "preferred_language", _.map(languagesSupported, 'value'), { listStyle: 2 })
    },
    (session, results) => {
      const language = _.find(languagesSupported, { value: results.response.entity })
      const locale = language && language.key
      session.preferredLocale(locale, (err) => {
        if (!err) {
          session.endDialog('language_selected', results.response.entity)
        } else {
          session.error(err)
        }
      })
    }
  ])
}

module.exports = {
  init: changeLanguage
}